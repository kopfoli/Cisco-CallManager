PRTG Device Template for Cisco Call Manager components
===========================================


This project contains all the files necessary to integrate the Cisco Call Manager
into PRTG for auto discovery and sensor creation.


Download Instructions
=========================
 A zip file containing all the files in the project can be downloaded from the 
repository(https://gitlab.com/PRTG-Projects/Device-Templates/Cisco-CallManager/-/jobs/artifacts/master/download?job=PRTGDistZip) 
download link


It is only tested with UCCX, CUCM devices
There are two Templates:

"Call Manager (Cisco CUIC)"
=====
![Cluster Status](./Images/ClusterStatus.png)

![Unity Globals](./Images/Unity_Globals.png)

![CUIC_ThreadPool_Info](./Images/CUIC_ThreadPool_Info.png)


![CUIC DB Info](./Images/CUIC_DB_Info.png)

![CUIC Reporting](./Images/CUIC_Reporting.png)

![CUIC Scheduler](./Images/CUIC_Scheduler.png)

![CUIC DataSource JMS](./Images/CUIC_DataSource_JMS.png)

![CUIC AppServer(Java)](./Images/CUIC_AppServer(Java).png)

"Call Manager Detail (Cisco CUIC)"
=====
![Detail Gateway Info](./Images/Detail_Gateway_Info.png)

![Media Device](./Images/Detail_MediaDevice.png)

![Phone Info](./Images/Detail_PhoneInfo.png)

Installation Instructions
=========================
Please refer to INSTALL.md

Known Issues
============
 Windows based Call Manager servers do not publish any information through SNMP and can therefore not be accessed.
